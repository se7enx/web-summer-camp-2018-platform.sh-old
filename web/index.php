<?php

echo 'The sun is shining<br />';

$line = date('c') . ' ' . $_SERVER['REQUEST_URI'];
$logFile = '/app/logs/access.log';
file_put_contents($logFile, $line . PHP_EOL , FILE_APPEND | LOCK_EX);

$db = null;
$relationships = getenv('PLATFORM_RELATIONSHIPS');
if ($relationships) {
    $relationships = json_decode(base64_decode($relationships), true);
    if (isset($relationships['db-main'])) {
        $endpoint = $relationships['db-main'][0];

        $dsn = "{$endpoint['scheme']}:dbname={$endpoint['path']};host={$endpoint['host']};port={$endpoint['port']}";
        $db = new PDO($dsn, $endpoint['username'], $endpoint['password']);
    }
}

if ($db !== null) {
    $query = 'SELECT name FROM fruits ORDER BY name ASC';
    $sth = $db->prepare($query);
    $sth->execute();
    $fruits = $sth->fetchAll(PDO::FETCH_COLUMN, 0);
    if (count($fruits)) {
        echo 'And very soon we will get: ' . implode('s, ', $fruits) .  's and much more<br />';
    }
}

echo 'My favorite fruit is: ' . getenv('FAVORITE_FRUIT');
